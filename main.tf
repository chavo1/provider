terraform {
  required_providers {
    myprovider = {
      source = "app.terraform.io/tfc4b/myprovider"
      version = "0.1.0"
    }
  }
}
